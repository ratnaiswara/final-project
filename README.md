# final-project

final project slytherin

STEP BY STEP CARA NGECLONE
1. Buka git bash
2. Clone dengan ketik "git clone https://gitlab.com/ratnaiswara/final-project.git"
3. Kerjakan bagian masing - masing
4. Setelah selesai, ketik "git add . "
5. Lalu ketik git commit -m "apa pesan commitnya"
6. Terakhir, ketik git push origin master

UNTUK BUAT BRANCH
1. git branch nama_branch (menambah branch)
2. git branch (untuk melihat branch yang ada, dan melihat branch yang sedang aktif)
3. git checkout nama_branch (untuk berpindah branch)
4. pergi ke branch master lalu "git merge nama_branch"

Note : Jika terjadi conflic kita perlu menghandel nya, caranya;
1. buka file yang bentrok ke text editor
2. nanti akan terlihat hal yang bentrok dan di pisahkan dengan tanda "====="
3. pilih yang ingin di ambil, dan hapus yang tidak perlu
4. simpan dan commit perubahan git add nama_file dan git commit -m "pesan perbaikan conflic"

Note : jika branch sudah tidak aktif atau tidak ada perubahan lagi kita bisa menghapusnya caranya ;
1. git branch -d nama_branch

UNTUK PULL (mengambil update terbaru)
1. "git pull https://gitlab.com/ratnaiswara/final-project.git"
