<?php 
class Admin extends CI_Controller{

    public function index(){
        $this->load->model("M_anggota");
        $data['title'] = "Admin | Home";
        $data['aktif1'] = "active";
        $data['aktif2'] = "";
        $data['aktif3'] = "";
        $data['anggota'] = $this->M_anggota->getall();
        $this->load->view('admin/template/header',$data);
        $this->load->view('admin/template/slidebar',$data);
        $this->load->view('admin/index');
        $this->load->view('admin/template/footer');
    }

    public function anggota(){
        $this->load->model("M_anggota");
        $data['title'] = "Admin | Anggota";
        $data['aktif1'] = "";
        $data['aktif2'] = "active";
        $data['aktif3'] = "";
        $data['anggota'] = $this->M_anggota->getall();
        $this->load->view('admin/template/header',$data);
        $this->load->view('admin/template/slidebar',$data);
        $this->load->view('admin/anggota');
        $this->load->view('admin/template/footer');
    }

    public function about(){
        $this->load->model("M_anggota");
        $data['title'] = "Admin | About";
        $data['aktif1'] = "";
        $data['aktif2'] = "";
        $data['aktif3'] = "active";
        $data['anggota'] = $this->M_anggota->getall();
        $this->load->view('admin/template/header',$data);
        $this->load->view('admin/template/slidebar',$data);
        $this->load->view('admin/about');
        $this->load->view('admin/template/footer');
    }
}