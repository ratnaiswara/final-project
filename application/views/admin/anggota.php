<div class="content-wrapper">
<div class="container mt-3">
      <form>
        <div class="mb-3">
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Telusuri Anggota">
        </div>
        <button type="submit" class="btn btn-success">Cari</button>
      </form>

    <table class="table text-center">

            <thead>
                <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Prodi</th>
                <th scope="col">House</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1;?>
                <?php foreach ($anggota as $angg) :?>
                <tr>
                <th scope="row"> <?= $i?> </th>
                <td> <?= $angg['nama_lengkap']; ?> </td>
                <td> <?= $angg['prodi']; ?> </td>
                <td>Slytherin</td>
                <td>
                  <a href="#" class="btn btn-primary">Detail</a>
                  <a href="#" class="btn btn-success">Edit</a>
                  <a href="#" class="btn btn-danger">Delete</a>
                </td>
                </tr>
                <?php $i++;?>
                <?php endforeach;?>
            </tbody>

    </table>
  </div>
</div>