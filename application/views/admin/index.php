<div class="content-wrapper">
<div class="container-fluid mt-3">

    <div class=""> 
        <!-- d-flex justify-content-around align-items-center -->
        <section class="content"> <!--col-md-6-->
        <div class="card text-center">
        <div class="card-header">
            <b>Informasi Sistem</b>
        </div>
        <div class="card-body">
            <ol>
                <li> Sistem Masih dalam pengembangan.</li>
                <li> Masih Belum Dapat melakukan CRUD.</li>
                <li> Tidak dapat upload gambar.</li>
            </ol>
            <div class="container-fluid house">
            <div class="row text-center">
                <div class="col-md-3 box-red">Gryffindor <br> <span class="num">5</span> </div>
                <div class="col-md-3 box-green">Slytherin <br> <span class="num">5</span> </div>
                <div class="col-md-3 box-blue">Ravenclaw <br> <span class="num">5</span> </div>
                <div class="col-md-3 box-yellow">Hufflepuff <br> <span class="num">5</span> </div>
                <div class="col-md-12 box-def">Semua Anggota <br> <span class="num">20</span> </div>
            </div>
            </div>
        </div>
        <div class="card-footer text-muted">
            2 days ago
        </div>
        </div>
        </section>

        <!-- <section class="content-index mt-3"> -->
            
        <!-- </section> -->
    </div>

    <section class="content-index mt-3">
    <div class="card text-center">
    <div class="card-header">
    <b>Daftar Anggota</b>
    </div>
    <div class="card-body">
        <!-- <h5 class="card-title">Daftar Anggota</h5> -->
        <table class="table table-dark table-striped mb-3">
            <thead>
                <tr>
                <th scope="col">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Prodi</th>
                <th scope="col">House</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($anggota as $angg) :?>
                <?php $i = 1;?>
                <tr>
                <th scope="row"> <?= $i?> </th>
                <td><?= $angg['nama_lengkap']?></td>
                <td><?= $angg['prodi']?></td>
                <td>Slytherin</td>
                </tr>
                <?php $i++?>
                <?php endforeach;?>
            </tbody>
            </table>
        <a href="#" class="btn btn-primary">Go somewhere</a>
    </div>
        <div class="card-footer text-muted">
            2 days ago
        </div>
    </div>
    </section>

</div>
</div>